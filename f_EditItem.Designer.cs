﻿namespace Milestone_4
{
    partial class f_EditItem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tb_ItemNameEdit = new System.Windows.Forms.TextBox();
            this.nud_ItemPriceEdit = new System.Windows.Forms.NumericUpDown();
            this.nud_ItemStockEdit = new System.Windows.Forms.NumericUpDown();
            this.nud_ItemWeightEdit = new System.Windows.Forms.NumericUpDown();
            this.nud_ItemSizeEdit = new System.Windows.Forms.NumericUpDown();
            this.tb_ItemDescriptionEdit = new System.Windows.Forms.TextBox();
            this.tl_ItemName = new System.Windows.Forms.Label();
            this.tl_ItemPrice = new System.Windows.Forms.Label();
            this.tl_ItemStock = new System.Windows.Forms.Label();
            this.tl_ItemWeight = new System.Windows.Forms.Label();
            this.tl_ItemSize = new System.Windows.Forms.Label();
            this.tl_ItemDescription = new System.Windows.Forms.Label();
            this.btn_SaveEdits = new System.Windows.Forms.Button();
            this.btn_CancelEdits = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.nud_ItemPriceEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_ItemStockEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_ItemWeightEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_ItemSizeEdit)).BeginInit();
            this.SuspendLayout();
            // 
            // tb_ItemNameEdit
            // 
            this.tb_ItemNameEdit.Location = new System.Drawing.Point(128, 12);
            this.tb_ItemNameEdit.Name = "tb_ItemNameEdit";
            this.tb_ItemNameEdit.Size = new System.Drawing.Size(124, 20);
            this.tb_ItemNameEdit.TabIndex = 0;
            // 
            // nud_ItemPriceEdit
            // 
            this.nud_ItemPriceEdit.DecimalPlaces = 2;
            this.nud_ItemPriceEdit.Location = new System.Drawing.Point(129, 38);
            this.nud_ItemPriceEdit.Name = "nud_ItemPriceEdit";
            this.nud_ItemPriceEdit.Size = new System.Drawing.Size(123, 20);
            this.nud_ItemPriceEdit.TabIndex = 1;
            // 
            // nud_ItemStockEdit
            // 
            this.nud_ItemStockEdit.Location = new System.Drawing.Point(129, 66);
            this.nud_ItemStockEdit.Name = "nud_ItemStockEdit";
            this.nud_ItemStockEdit.Size = new System.Drawing.Size(123, 20);
            this.nud_ItemStockEdit.TabIndex = 2;
            // 
            // nud_ItemWeightEdit
            // 
            this.nud_ItemWeightEdit.DecimalPlaces = 3;
            this.nud_ItemWeightEdit.Location = new System.Drawing.Point(129, 92);
            this.nud_ItemWeightEdit.Name = "nud_ItemWeightEdit";
            this.nud_ItemWeightEdit.Size = new System.Drawing.Size(123, 20);
            this.nud_ItemWeightEdit.TabIndex = 3;
            // 
            // nud_ItemSizeEdit
            // 
            this.nud_ItemSizeEdit.DecimalPlaces = 3;
            this.nud_ItemSizeEdit.Location = new System.Drawing.Point(129, 118);
            this.nud_ItemSizeEdit.Name = "nud_ItemSizeEdit";
            this.nud_ItemSizeEdit.Size = new System.Drawing.Size(123, 20);
            this.nud_ItemSizeEdit.TabIndex = 4;
            // 
            // tb_ItemDescriptionEdit
            // 
            this.tb_ItemDescriptionEdit.Location = new System.Drawing.Point(129, 144);
            this.tb_ItemDescriptionEdit.Multiline = true;
            this.tb_ItemDescriptionEdit.Name = "tb_ItemDescriptionEdit";
            this.tb_ItemDescriptionEdit.Size = new System.Drawing.Size(123, 70);
            this.tb_ItemDescriptionEdit.TabIndex = 5;
            // 
            // tl_ItemName
            // 
            this.tl_ItemName.AutoSize = true;
            this.tl_ItemName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tl_ItemName.Location = new System.Drawing.Point(35, 12);
            this.tl_ItemName.Name = "tl_ItemName";
            this.tl_ItemName.Size = new System.Drawing.Size(47, 15);
            this.tl_ItemName.TabIndex = 6;
            this.tl_ItemName.Text = "Name: ";
            // 
            // tl_ItemPrice
            // 
            this.tl_ItemPrice.AutoSize = true;
            this.tl_ItemPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tl_ItemPrice.Location = new System.Drawing.Point(34, 40);
            this.tl_ItemPrice.Name = "tl_ItemPrice";
            this.tl_ItemPrice.Size = new System.Drawing.Size(41, 15);
            this.tl_ItemPrice.TabIndex = 7;
            this.tl_ItemPrice.Text = "Price: ";
            // 
            // tl_ItemStock
            // 
            this.tl_ItemStock.AutoSize = true;
            this.tl_ItemStock.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tl_ItemStock.Location = new System.Drawing.Point(34, 67);
            this.tl_ItemStock.Name = "tl_ItemStock";
            this.tl_ItemStock.Size = new System.Drawing.Size(40, 15);
            this.tl_ItemStock.TabIndex = 8;
            this.tl_ItemStock.Text = "Stock:";
            // 
            // tl_ItemWeight
            // 
            this.tl_ItemWeight.AutoSize = true;
            this.tl_ItemWeight.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tl_ItemWeight.Location = new System.Drawing.Point(34, 94);
            this.tl_ItemWeight.Name = "tl_ItemWeight";
            this.tl_ItemWeight.Size = new System.Drawing.Size(48, 15);
            this.tl_ItemWeight.TabIndex = 9;
            this.tl_ItemWeight.Text = "Weight:";
            // 
            // tl_ItemSize
            // 
            this.tl_ItemSize.AutoSize = true;
            this.tl_ItemSize.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tl_ItemSize.Location = new System.Drawing.Point(35, 120);
            this.tl_ItemSize.Name = "tl_ItemSize";
            this.tl_ItemSize.Size = new System.Drawing.Size(34, 15);
            this.tl_ItemSize.TabIndex = 10;
            this.tl_ItemSize.Text = "Size:";
            // 
            // tl_ItemDescription
            // 
            this.tl_ItemDescription.AutoSize = true;
            this.tl_ItemDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tl_ItemDescription.Location = new System.Drawing.Point(34, 172);
            this.tl_ItemDescription.Name = "tl_ItemDescription";
            this.tl_ItemDescription.Size = new System.Drawing.Size(72, 15);
            this.tl_ItemDescription.TabIndex = 11;
            this.tl_ItemDescription.Text = "Description:";
            // 
            // btn_SaveEdits
            // 
            this.btn_SaveEdits.Location = new System.Drawing.Point(79, 238);
            this.btn_SaveEdits.Name = "btn_SaveEdits";
            this.btn_SaveEdits.Size = new System.Drawing.Size(124, 50);
            this.btn_SaveEdits.TabIndex = 12;
            this.btn_SaveEdits.Text = "Save";
            this.btn_SaveEdits.UseVisualStyleBackColor = true;
            this.btn_SaveEdits.Click += new System.EventHandler(this.btn_SaveEdits_Click);
            // 
            // btn_CancelEdits
            // 
            this.btn_CancelEdits.Location = new System.Drawing.Point(79, 294);
            this.btn_CancelEdits.Name = "btn_CancelEdits";
            this.btn_CancelEdits.Size = new System.Drawing.Size(124, 27);
            this.btn_CancelEdits.TabIndex = 13;
            this.btn_CancelEdits.Text = "Cancel";
            this.btn_CancelEdits.UseVisualStyleBackColor = true;
            this.btn_CancelEdits.Click += new System.EventHandler(this.btn_CancelEdits_Click);
            // 
            // f_EditItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(288, 333);
            this.Controls.Add(this.btn_CancelEdits);
            this.Controls.Add(this.btn_SaveEdits);
            this.Controls.Add(this.tl_ItemDescription);
            this.Controls.Add(this.tl_ItemSize);
            this.Controls.Add(this.tl_ItemWeight);
            this.Controls.Add(this.tl_ItemStock);
            this.Controls.Add(this.tl_ItemPrice);
            this.Controls.Add(this.tl_ItemName);
            this.Controls.Add(this.tb_ItemDescriptionEdit);
            this.Controls.Add(this.nud_ItemSizeEdit);
            this.Controls.Add(this.nud_ItemWeightEdit);
            this.Controls.Add(this.nud_ItemStockEdit);
            this.Controls.Add(this.nud_ItemPriceEdit);
            this.Controls.Add(this.tb_ItemNameEdit);
            this.Name = "f_EditItem";
            this.Text = "Edit - Item";
            this.Load += new System.EventHandler(this.f_EditItem_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nud_ItemPriceEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_ItemStockEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_ItemWeightEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_ItemSizeEdit)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tb_ItemNameEdit;
        private System.Windows.Forms.NumericUpDown nud_ItemPriceEdit;
        private System.Windows.Forms.NumericUpDown nud_ItemStockEdit;
        private System.Windows.Forms.NumericUpDown nud_ItemWeightEdit;
        private System.Windows.Forms.NumericUpDown nud_ItemSizeEdit;
        private System.Windows.Forms.TextBox tb_ItemDescriptionEdit;
        private System.Windows.Forms.Label tl_ItemName;
        private System.Windows.Forms.Label tl_ItemPrice;
        private System.Windows.Forms.Label tl_ItemStock;
        private System.Windows.Forms.Label tl_ItemWeight;
        private System.Windows.Forms.Label tl_ItemSize;
        private System.Windows.Forms.Label tl_ItemDescription;
        private System.Windows.Forms.Button btn_SaveEdits;
        private System.Windows.Forms.Button btn_CancelEdits;
    }
}