﻿namespace Milestone_4
{
    partial class f_InventoryManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_AddItem = new System.Windows.Forms.Button();
            this.rb_SortLowPrice = new System.Windows.Forms.RadioButton();
            this.rb_SortHighPrice = new System.Windows.Forms.RadioButton();
            this.rb_HighestStock = new System.Windows.Forms.RadioButton();
            this.rb_LowestStock = new System.Windows.Forms.RadioButton();
            this.rb_Lightest = new System.Windows.Forms.RadioButton();
            this.rb_Heaviest = new System.Windows.Forms.RadioButton();
            this.rb_Smallest = new System.Windows.Forms.RadioButton();
            this.rb_Biggest = new System.Windows.Forms.RadioButton();
            this.gb_SortByGroup = new System.Windows.Forms.GroupBox();
            this.rb_Alphabetics = new System.Windows.Forms.RadioButton();
            this.btn_PreviousPage = new System.Windows.Forms.Button();
            this.btn_NextPage = new System.Windows.Forms.Button();
            this.tl_Pages = new System.Windows.Forms.Label();
            this.tl_Name = new System.Windows.Forms.Label();
            this.tl_Price = new System.Windows.Forms.Label();
            this.tl_Stock = new System.Windows.Forms.Label();
            this.tl_Weight = new System.Windows.Forms.Label();
            this.tl_Size = new System.Windows.Forms.Label();
            this.tl_Description = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.nud_ItemStock1 = new System.Windows.Forms.NumericUpDown();
            this.tl_ItemName1 = new System.Windows.Forms.Label();
            this.tl_ItemName2 = new System.Windows.Forms.Label();
            this.tl_ItemName3 = new System.Windows.Forms.Label();
            this.tl_ItemPrice1 = new System.Windows.Forms.Label();
            this.tl_ItemName4 = new System.Windows.Forms.Label();
            this.tl_ItemPrice2 = new System.Windows.Forms.Label();
            this.tl_ItemPrice3 = new System.Windows.Forms.Label();
            this.tl_ItemPrice4 = new System.Windows.Forms.Label();
            this.tl_ItemWeight1 = new System.Windows.Forms.Label();
            this.tl_ItemWeight2 = new System.Windows.Forms.Label();
            this.tl_ItemWeight3 = new System.Windows.Forms.Label();
            this.tl_ItemWeight4 = new System.Windows.Forms.Label();
            this.tl_ItemSize1 = new System.Windows.Forms.Label();
            this.tl_ItemSize2 = new System.Windows.Forms.Label();
            this.tl_ItemSize3 = new System.Windows.Forms.Label();
            this.tl_ItemSize4 = new System.Windows.Forms.Label();
            this.tl_ItemDescription2 = new System.Windows.Forms.Label();
            this.tl_ItemDescription3 = new System.Windows.Forms.Label();
            this.tl_ItemDescription4 = new System.Windows.Forms.Label();
            this.tl_ItemDescription1 = new System.Windows.Forms.Label();
            this.tl_Edit = new System.Windows.Forms.Label();
            this.tl_Delete = new System.Windows.Forms.Label();
            this.btn_ItemEdit1 = new System.Windows.Forms.Button();
            this.btn_ItemEdit2 = new System.Windows.Forms.Button();
            this.btn_ItemEdit3 = new System.Windows.Forms.Button();
            this.btn_ItemEdit4 = new System.Windows.Forms.Button();
            this.btn_ItemDelete1 = new System.Windows.Forms.Button();
            this.btn_ItemDelete2 = new System.Windows.Forms.Button();
            this.btn_ItemDelete3 = new System.Windows.Forms.Button();
            this.btn_ItemDelete4 = new System.Windows.Forms.Button();
            this.nud_ItemStock2 = new System.Windows.Forms.NumericUpDown();
            this.nud_ItemStock3 = new System.Windows.Forms.NumericUpDown();
            this.nud_ItemStock4 = new System.Windows.Forms.NumericUpDown();
            this.btn_RefreshPage = new System.Windows.Forms.Button();
            this.gb_SortByGroup.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nud_ItemStock1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_ItemStock2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_ItemStock3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_ItemStock4)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_AddItem
            // 
            this.btn_AddItem.Location = new System.Drawing.Point(12, 12);
            this.btn_AddItem.Name = "btn_AddItem";
            this.btn_AddItem.Size = new System.Drawing.Size(129, 35);
            this.btn_AddItem.TabIndex = 0;
            this.btn_AddItem.Text = "Add New Item";
            this.btn_AddItem.UseVisualStyleBackColor = true;
            this.btn_AddItem.Click += new System.EventHandler(this.btn_AddItem_Click);
            // 
            // rb_SortLowPrice
            // 
            this.rb_SortLowPrice.AutoSize = true;
            this.rb_SortLowPrice.Location = new System.Drawing.Point(13, 66);
            this.rb_SortLowPrice.Name = "rb_SortLowPrice";
            this.rb_SortLowPrice.Size = new System.Drawing.Size(86, 17);
            this.rb_SortLowPrice.TabIndex = 4;
            this.rb_SortLowPrice.TabStop = true;
            this.rb_SortLowPrice.Text = "Lowest Price";
            this.rb_SortLowPrice.UseVisualStyleBackColor = true;
            this.rb_SortLowPrice.CheckedChanged += new System.EventHandler(this.rb_SortLowPrice_CheckedChanged);
            // 
            // rb_SortHighPrice
            // 
            this.rb_SortHighPrice.AutoSize = true;
            this.rb_SortHighPrice.Location = new System.Drawing.Point(13, 43);
            this.rb_SortHighPrice.Name = "rb_SortHighPrice";
            this.rb_SortHighPrice.Size = new System.Drawing.Size(88, 17);
            this.rb_SortHighPrice.TabIndex = 5;
            this.rb_SortHighPrice.TabStop = true;
            this.rb_SortHighPrice.Text = "Highest Price";
            this.rb_SortHighPrice.UseVisualStyleBackColor = true;
            this.rb_SortHighPrice.CheckedChanged += new System.EventHandler(this.rb_SortHighPrice_CheckedChanged);
            // 
            // rb_HighestStock
            // 
            this.rb_HighestStock.AutoSize = true;
            this.rb_HighestStock.Location = new System.Drawing.Point(13, 89);
            this.rb_HighestStock.Name = "rb_HighestStock";
            this.rb_HighestStock.Size = new System.Drawing.Size(92, 17);
            this.rb_HighestStock.TabIndex = 5;
            this.rb_HighestStock.TabStop = true;
            this.rb_HighestStock.Text = "Highest Stock";
            this.rb_HighestStock.UseVisualStyleBackColor = true;
            this.rb_HighestStock.CheckedChanged += new System.EventHandler(this.rb_HighestStock_CheckedChanged);
            // 
            // rb_LowestStock
            // 
            this.rb_LowestStock.AutoSize = true;
            this.rb_LowestStock.Location = new System.Drawing.Point(13, 112);
            this.rb_LowestStock.Name = "rb_LowestStock";
            this.rb_LowestStock.Size = new System.Drawing.Size(90, 17);
            this.rb_LowestStock.TabIndex = 4;
            this.rb_LowestStock.TabStop = true;
            this.rb_LowestStock.Text = "Lowest Stock";
            this.rb_LowestStock.UseVisualStyleBackColor = true;
            this.rb_LowestStock.CheckedChanged += new System.EventHandler(this.rb_LowestStock_CheckedChanged);
            // 
            // rb_Lightest
            // 
            this.rb_Lightest.AutoSize = true;
            this.rb_Lightest.Location = new System.Drawing.Point(13, 134);
            this.rb_Lightest.Name = "rb_Lightest";
            this.rb_Lightest.Size = new System.Drawing.Size(62, 17);
            this.rb_Lightest.TabIndex = 5;
            this.rb_Lightest.TabStop = true;
            this.rb_Lightest.Text = "Lightest";
            this.rb_Lightest.UseVisualStyleBackColor = true;
            this.rb_Lightest.CheckedChanged += new System.EventHandler(this.rb_Lightest_CheckedChanged);
            // 
            // rb_Heaviest
            // 
            this.rb_Heaviest.AutoSize = true;
            this.rb_Heaviest.Location = new System.Drawing.Point(13, 157);
            this.rb_Heaviest.Name = "rb_Heaviest";
            this.rb_Heaviest.Size = new System.Drawing.Size(67, 17);
            this.rb_Heaviest.TabIndex = 4;
            this.rb_Heaviest.TabStop = true;
            this.rb_Heaviest.Text = "Heaviest";
            this.rb_Heaviest.UseVisualStyleBackColor = true;
            this.rb_Heaviest.CheckedChanged += new System.EventHandler(this.rb_Heaviest_CheckedChanged);
            // 
            // rb_Smallest
            // 
            this.rb_Smallest.AutoSize = true;
            this.rb_Smallest.Location = new System.Drawing.Point(13, 180);
            this.rb_Smallest.Name = "rb_Smallest";
            this.rb_Smallest.Size = new System.Drawing.Size(64, 17);
            this.rb_Smallest.TabIndex = 5;
            this.rb_Smallest.TabStop = true;
            this.rb_Smallest.Text = "Smallest";
            this.rb_Smallest.UseVisualStyleBackColor = true;
            this.rb_Smallest.CheckedChanged += new System.EventHandler(this.rb_Smallest_CheckedChanged);
            // 
            // rb_Biggest
            // 
            this.rb_Biggest.AutoSize = true;
            this.rb_Biggest.Location = new System.Drawing.Point(13, 203);
            this.rb_Biggest.Name = "rb_Biggest";
            this.rb_Biggest.Size = new System.Drawing.Size(60, 17);
            this.rb_Biggest.TabIndex = 4;
            this.rb_Biggest.TabStop = true;
            this.rb_Biggest.Text = "Biggest";
            this.rb_Biggest.UseVisualStyleBackColor = true;
            this.rb_Biggest.CheckedChanged += new System.EventHandler(this.rb_Biggest_CheckedChanged);
            // 
            // gb_SortByGroup
            // 
            this.gb_SortByGroup.Controls.Add(this.rb_Biggest);
            this.gb_SortByGroup.Controls.Add(this.rb_Smallest);
            this.gb_SortByGroup.Controls.Add(this.rb_SortHighPrice);
            this.gb_SortByGroup.Controls.Add(this.rb_Alphabetics);
            this.gb_SortByGroup.Controls.Add(this.rb_Heaviest);
            this.gb_SortByGroup.Controls.Add(this.rb_Lightest);
            this.gb_SortByGroup.Controls.Add(this.rb_SortLowPrice);
            this.gb_SortByGroup.Controls.Add(this.rb_HighestStock);
            this.gb_SortByGroup.Controls.Add(this.rb_LowestStock);
            this.gb_SortByGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gb_SortByGroup.Location = new System.Drawing.Point(12, 94);
            this.gb_SortByGroup.Name = "gb_SortByGroup";
            this.gb_SortByGroup.Size = new System.Drawing.Size(129, 236);
            this.gb_SortByGroup.TabIndex = 11;
            this.gb_SortByGroup.TabStop = false;
            this.gb_SortByGroup.Text = "Sort By";
            // 
            // rb_Alphabetics
            // 
            this.rb_Alphabetics.AutoSize = true;
            this.rb_Alphabetics.Checked = true;
            this.rb_Alphabetics.Location = new System.Drawing.Point(13, 20);
            this.rb_Alphabetics.Name = "rb_Alphabetics";
            this.rb_Alphabetics.Size = new System.Drawing.Size(90, 17);
            this.rb_Alphabetics.TabIndex = 9;
            this.rb_Alphabetics.TabStop = true;
            this.rb_Alphabetics.Text = "Alphabetically";
            this.rb_Alphabetics.UseVisualStyleBackColor = true;
            this.rb_Alphabetics.CheckedChanged += new System.EventHandler(this.rb_Alphabetics_CheckedChanged);
            // 
            // btn_PreviousPage
            // 
            this.btn_PreviousPage.Location = new System.Drawing.Point(295, 405);
            this.btn_PreviousPage.Name = "btn_PreviousPage";
            this.btn_PreviousPage.Size = new System.Drawing.Size(75, 23);
            this.btn_PreviousPage.TabIndex = 13;
            this.btn_PreviousPage.Text = "Last";
            this.btn_PreviousPage.UseVisualStyleBackColor = true;
            this.btn_PreviousPage.Click += new System.EventHandler(this.btn_PreviousPage_Click);
            // 
            // btn_NextPage
            // 
            this.btn_NextPage.Location = new System.Drawing.Point(424, 405);
            this.btn_NextPage.Name = "btn_NextPage";
            this.btn_NextPage.Size = new System.Drawing.Size(75, 23);
            this.btn_NextPage.TabIndex = 14;
            this.btn_NextPage.Text = "Next";
            this.btn_NextPage.UseVisualStyleBackColor = true;
            this.btn_NextPage.Click += new System.EventHandler(this.btn_NextPage_Click);
            // 
            // tl_Pages
            // 
            this.tl_Pages.AutoSize = true;
            this.tl_Pages.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tl_Pages.Location = new System.Drawing.Point(385, 408);
            this.tl_Pages.Name = "tl_Pages";
            this.tl_Pages.Size = new System.Drawing.Size(25, 17);
            this.tl_Pages.TabIndex = 0;
            this.tl_Pages.Text = "-/-";
            // 
            // tl_Name
            // 
            this.tl_Name.AutoSize = true;
            this.tl_Name.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tl_Name.Location = new System.Drawing.Point(3, 0);
            this.tl_Name.Name = "tl_Name";
            this.tl_Name.Size = new System.Drawing.Size(49, 17);
            this.tl_Name.TabIndex = 15;
            this.tl_Name.Text = "Name";
            // 
            // tl_Price
            // 
            this.tl_Price.AutoSize = true;
            this.tl_Price.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tl_Price.Location = new System.Drawing.Point(134, 0);
            this.tl_Price.Name = "tl_Price";
            this.tl_Price.Size = new System.Drawing.Size(45, 17);
            this.tl_Price.TabIndex = 16;
            this.tl_Price.Text = "Price";
            // 
            // tl_Stock
            // 
            this.tl_Stock.AutoSize = true;
            this.tl_Stock.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tl_Stock.Location = new System.Drawing.Point(195, 0);
            this.tl_Stock.Name = "tl_Stock";
            this.tl_Stock.Size = new System.Drawing.Size(48, 17);
            this.tl_Stock.TabIndex = 17;
            this.tl_Stock.Text = "Stock";
            // 
            // tl_Weight
            // 
            this.tl_Weight.AutoSize = true;
            this.tl_Weight.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tl_Weight.Location = new System.Drawing.Point(249, 0);
            this.tl_Weight.Name = "tl_Weight";
            this.tl_Weight.Size = new System.Drawing.Size(58, 17);
            this.tl_Weight.TabIndex = 18;
            this.tl_Weight.Text = "Weight";
            // 
            // tl_Size
            // 
            this.tl_Size.AutoSize = true;
            this.tl_Size.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tl_Size.Location = new System.Drawing.Point(314, 0);
            this.tl_Size.Name = "tl_Size";
            this.tl_Size.Size = new System.Drawing.Size(39, 17);
            this.tl_Size.TabIndex = 19;
            this.tl_Size.Text = "Size";
            // 
            // tl_Description
            // 
            this.tl_Description.AutoSize = true;
            this.tl_Description.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tl_Description.Location = new System.Drawing.Point(359, 0);
            this.tl_Description.Name = "tl_Description";
            this.tl_Description.Size = new System.Drawing.Size(90, 17);
            this.tl_Description.TabIndex = 20;
            this.tl_Description.Text = "Description";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 8;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 68.04124F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 31.95876F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 54F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 65F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 136F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 42F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 66F));
            this.tableLayoutPanel1.Controls.Add(this.nud_ItemStock1, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.tl_Name, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tl_Price, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tl_Description, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.tl_Stock, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.tl_Size, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.tl_ItemName1, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.tl_ItemName2, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.tl_ItemName3, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.tl_ItemPrice1, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.tl_ItemName4, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.tl_ItemPrice2, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.tl_ItemPrice3, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.tl_ItemPrice4, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.tl_ItemWeight1, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.tl_ItemWeight2, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.tl_ItemWeight3, 3, 4);
            this.tableLayoutPanel1.Controls.Add(this.tl_ItemWeight4, 3, 5);
            this.tableLayoutPanel1.Controls.Add(this.tl_ItemSize1, 4, 2);
            this.tableLayoutPanel1.Controls.Add(this.tl_ItemSize2, 4, 3);
            this.tableLayoutPanel1.Controls.Add(this.tl_ItemSize3, 4, 4);
            this.tableLayoutPanel1.Controls.Add(this.tl_ItemSize4, 4, 5);
            this.tableLayoutPanel1.Controls.Add(this.tl_ItemDescription2, 5, 3);
            this.tableLayoutPanel1.Controls.Add(this.tl_ItemDescription3, 5, 4);
            this.tableLayoutPanel1.Controls.Add(this.tl_ItemDescription4, 5, 5);
            this.tableLayoutPanel1.Controls.Add(this.tl_Weight, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.tl_ItemDescription1, 5, 2);
            this.tableLayoutPanel1.Controls.Add(this.tl_Edit, 6, 0);
            this.tableLayoutPanel1.Controls.Add(this.tl_Delete, 7, 0);
            this.tableLayoutPanel1.Controls.Add(this.btn_ItemEdit1, 6, 2);
            this.tableLayoutPanel1.Controls.Add(this.btn_ItemEdit2, 6, 3);
            this.tableLayoutPanel1.Controls.Add(this.btn_ItemEdit3, 6, 4);
            this.tableLayoutPanel1.Controls.Add(this.btn_ItemEdit4, 6, 5);
            this.tableLayoutPanel1.Controls.Add(this.btn_ItemDelete1, 7, 2);
            this.tableLayoutPanel1.Controls.Add(this.btn_ItemDelete2, 7, 3);
            this.tableLayoutPanel1.Controls.Add(this.btn_ItemDelete3, 7, 4);
            this.tableLayoutPanel1.Controls.Add(this.btn_ItemDelete4, 7, 5);
            this.tableLayoutPanel1.Controls.Add(this.nud_ItemStock2, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.nud_ItemStock3, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.nud_ItemStock4, 2, 5);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(147, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 39.21569F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60.78431F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 81F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 84F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 85F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 81F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(602, 383);
            this.tableLayoutPanel1.TabIndex = 22;
            // 
            // nud_ItemStock1
            // 
            this.nud_ItemStock1.Location = new System.Drawing.Point(195, 54);
            this.nud_ItemStock1.Name = "nud_ItemStock1";
            this.nud_ItemStock1.Size = new System.Drawing.Size(46, 20);
            this.nud_ItemStock1.TabIndex = 24;
            // 
            // tl_ItemName1
            // 
            this.tl_ItemName1.AutoSize = true;
            this.tl_ItemName1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tl_ItemName1.Location = new System.Drawing.Point(3, 51);
            this.tl_ItemName1.Name = "tl_ItemName1";
            this.tl_ItemName1.Size = new System.Drawing.Size(13, 17);
            this.tl_ItemName1.TabIndex = 23;
            this.tl_ItemName1.Text = "-";
            // 
            // tl_ItemName2
            // 
            this.tl_ItemName2.AutoSize = true;
            this.tl_ItemName2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tl_ItemName2.Location = new System.Drawing.Point(3, 132);
            this.tl_ItemName2.Name = "tl_ItemName2";
            this.tl_ItemName2.Size = new System.Drawing.Size(13, 17);
            this.tl_ItemName2.TabIndex = 24;
            this.tl_ItemName2.Text = "-";
            // 
            // tl_ItemName3
            // 
            this.tl_ItemName3.AutoSize = true;
            this.tl_ItemName3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tl_ItemName3.Location = new System.Drawing.Point(3, 216);
            this.tl_ItemName3.Name = "tl_ItemName3";
            this.tl_ItemName3.Size = new System.Drawing.Size(13, 17);
            this.tl_ItemName3.TabIndex = 25;
            this.tl_ItemName3.Text = "-";
            // 
            // tl_ItemPrice1
            // 
            this.tl_ItemPrice1.AutoSize = true;
            this.tl_ItemPrice1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tl_ItemPrice1.Location = new System.Drawing.Point(134, 51);
            this.tl_ItemPrice1.Name = "tl_ItemPrice1";
            this.tl_ItemPrice1.Size = new System.Drawing.Size(13, 17);
            this.tl_ItemPrice1.TabIndex = 27;
            this.tl_ItemPrice1.Text = "-";
            // 
            // tl_ItemName4
            // 
            this.tl_ItemName4.AutoSize = true;
            this.tl_ItemName4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tl_ItemName4.Location = new System.Drawing.Point(3, 301);
            this.tl_ItemName4.Name = "tl_ItemName4";
            this.tl_ItemName4.Size = new System.Drawing.Size(13, 17);
            this.tl_ItemName4.TabIndex = 26;
            this.tl_ItemName4.Text = "-";
            // 
            // tl_ItemPrice2
            // 
            this.tl_ItemPrice2.AutoSize = true;
            this.tl_ItemPrice2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tl_ItemPrice2.Location = new System.Drawing.Point(134, 132);
            this.tl_ItemPrice2.Name = "tl_ItemPrice2";
            this.tl_ItemPrice2.Size = new System.Drawing.Size(13, 17);
            this.tl_ItemPrice2.TabIndex = 28;
            this.tl_ItemPrice2.Text = "-";
            // 
            // tl_ItemPrice3
            // 
            this.tl_ItemPrice3.AutoSize = true;
            this.tl_ItemPrice3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tl_ItemPrice3.Location = new System.Drawing.Point(134, 216);
            this.tl_ItemPrice3.Name = "tl_ItemPrice3";
            this.tl_ItemPrice3.Size = new System.Drawing.Size(13, 17);
            this.tl_ItemPrice3.TabIndex = 29;
            this.tl_ItemPrice3.Text = "-";
            // 
            // tl_ItemPrice4
            // 
            this.tl_ItemPrice4.AutoSize = true;
            this.tl_ItemPrice4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tl_ItemPrice4.Location = new System.Drawing.Point(134, 301);
            this.tl_ItemPrice4.Name = "tl_ItemPrice4";
            this.tl_ItemPrice4.Size = new System.Drawing.Size(13, 17);
            this.tl_ItemPrice4.TabIndex = 30;
            this.tl_ItemPrice4.Text = "-";
            // 
            // tl_ItemWeight1
            // 
            this.tl_ItemWeight1.AutoSize = true;
            this.tl_ItemWeight1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tl_ItemWeight1.Location = new System.Drawing.Point(249, 51);
            this.tl_ItemWeight1.Name = "tl_ItemWeight1";
            this.tl_ItemWeight1.Size = new System.Drawing.Size(13, 17);
            this.tl_ItemWeight1.TabIndex = 35;
            this.tl_ItemWeight1.Text = "-";
            // 
            // tl_ItemWeight2
            // 
            this.tl_ItemWeight2.AutoSize = true;
            this.tl_ItemWeight2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tl_ItemWeight2.Location = new System.Drawing.Point(249, 132);
            this.tl_ItemWeight2.Name = "tl_ItemWeight2";
            this.tl_ItemWeight2.Size = new System.Drawing.Size(13, 17);
            this.tl_ItemWeight2.TabIndex = 36;
            this.tl_ItemWeight2.Text = "-";
            // 
            // tl_ItemWeight3
            // 
            this.tl_ItemWeight3.AutoSize = true;
            this.tl_ItemWeight3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tl_ItemWeight3.Location = new System.Drawing.Point(249, 216);
            this.tl_ItemWeight3.Name = "tl_ItemWeight3";
            this.tl_ItemWeight3.Size = new System.Drawing.Size(13, 17);
            this.tl_ItemWeight3.TabIndex = 37;
            this.tl_ItemWeight3.Text = "-";
            // 
            // tl_ItemWeight4
            // 
            this.tl_ItemWeight4.AutoSize = true;
            this.tl_ItemWeight4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tl_ItemWeight4.Location = new System.Drawing.Point(249, 301);
            this.tl_ItemWeight4.Name = "tl_ItemWeight4";
            this.tl_ItemWeight4.Size = new System.Drawing.Size(13, 17);
            this.tl_ItemWeight4.TabIndex = 38;
            this.tl_ItemWeight4.Text = "-";
            // 
            // tl_ItemSize1
            // 
            this.tl_ItemSize1.AutoSize = true;
            this.tl_ItemSize1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tl_ItemSize1.Location = new System.Drawing.Point(314, 51);
            this.tl_ItemSize1.Name = "tl_ItemSize1";
            this.tl_ItemSize1.Size = new System.Drawing.Size(13, 17);
            this.tl_ItemSize1.TabIndex = 39;
            this.tl_ItemSize1.Text = "-";
            // 
            // tl_ItemSize2
            // 
            this.tl_ItemSize2.AutoSize = true;
            this.tl_ItemSize2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tl_ItemSize2.Location = new System.Drawing.Point(314, 132);
            this.tl_ItemSize2.Name = "tl_ItemSize2";
            this.tl_ItemSize2.Size = new System.Drawing.Size(13, 17);
            this.tl_ItemSize2.TabIndex = 40;
            this.tl_ItemSize2.Text = "-";
            // 
            // tl_ItemSize3
            // 
            this.tl_ItemSize3.AutoSize = true;
            this.tl_ItemSize3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tl_ItemSize3.Location = new System.Drawing.Point(314, 216);
            this.tl_ItemSize3.Name = "tl_ItemSize3";
            this.tl_ItemSize3.Size = new System.Drawing.Size(13, 17);
            this.tl_ItemSize3.TabIndex = 41;
            this.tl_ItemSize3.Text = "-";
            // 
            // tl_ItemSize4
            // 
            this.tl_ItemSize4.AutoSize = true;
            this.tl_ItemSize4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tl_ItemSize4.Location = new System.Drawing.Point(314, 301);
            this.tl_ItemSize4.Name = "tl_ItemSize4";
            this.tl_ItemSize4.Size = new System.Drawing.Size(13, 17);
            this.tl_ItemSize4.TabIndex = 42;
            this.tl_ItemSize4.Text = "-";
            // 
            // tl_ItemDescription2
            // 
            this.tl_ItemDescription2.AutoSize = true;
            this.tl_ItemDescription2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tl_ItemDescription2.Location = new System.Drawing.Point(359, 132);
            this.tl_ItemDescription2.Name = "tl_ItemDescription2";
            this.tl_ItemDescription2.Size = new System.Drawing.Size(13, 17);
            this.tl_ItemDescription2.TabIndex = 44;
            this.tl_ItemDescription2.Text = "-";
            // 
            // tl_ItemDescription3
            // 
            this.tl_ItemDescription3.AutoSize = true;
            this.tl_ItemDescription3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tl_ItemDescription3.Location = new System.Drawing.Point(359, 216);
            this.tl_ItemDescription3.Name = "tl_ItemDescription3";
            this.tl_ItemDescription3.Size = new System.Drawing.Size(13, 17);
            this.tl_ItemDescription3.TabIndex = 45;
            this.tl_ItemDescription3.Text = "-";
            // 
            // tl_ItemDescription4
            // 
            this.tl_ItemDescription4.AutoSize = true;
            this.tl_ItemDescription4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tl_ItemDescription4.Location = new System.Drawing.Point(359, 301);
            this.tl_ItemDescription4.Name = "tl_ItemDescription4";
            this.tl_ItemDescription4.Size = new System.Drawing.Size(13, 17);
            this.tl_ItemDescription4.TabIndex = 46;
            this.tl_ItemDescription4.Text = "-";
            // 
            // tl_ItemDescription1
            // 
            this.tl_ItemDescription1.AutoSize = true;
            this.tl_ItemDescription1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tl_ItemDescription1.Location = new System.Drawing.Point(359, 51);
            this.tl_ItemDescription1.Name = "tl_ItemDescription1";
            this.tl_ItemDescription1.Size = new System.Drawing.Size(13, 17);
            this.tl_ItemDescription1.TabIndex = 43;
            this.tl_ItemDescription1.Text = "-";
            // 
            // tl_Edit
            // 
            this.tl_Edit.AutoSize = true;
            this.tl_Edit.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tl_Edit.Location = new System.Drawing.Point(495, 0);
            this.tl_Edit.Name = "tl_Edit";
            this.tl_Edit.Size = new System.Drawing.Size(36, 17);
            this.tl_Edit.TabIndex = 47;
            this.tl_Edit.Text = "Edit";
            // 
            // tl_Delete
            // 
            this.tl_Delete.AutoSize = true;
            this.tl_Delete.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tl_Delete.Location = new System.Drawing.Point(537, 0);
            this.tl_Delete.Name = "tl_Delete";
            this.tl_Delete.Size = new System.Drawing.Size(55, 17);
            this.tl_Delete.TabIndex = 48;
            this.tl_Delete.Text = "Delete";
            // 
            // btn_ItemEdit1
            // 
            this.btn_ItemEdit1.Location = new System.Drawing.Point(495, 54);
            this.btn_ItemEdit1.Name = "btn_ItemEdit1";
            this.btn_ItemEdit1.Size = new System.Drawing.Size(36, 23);
            this.btn_ItemEdit1.TabIndex = 49;
            this.btn_ItemEdit1.Text = "Edit";
            this.btn_ItemEdit1.UseVisualStyleBackColor = true;
            this.btn_ItemEdit1.Click += new System.EventHandler(this.btn_ItemEdit1_Click);
            // 
            // btn_ItemEdit2
            // 
            this.btn_ItemEdit2.Location = new System.Drawing.Point(495, 135);
            this.btn_ItemEdit2.Name = "btn_ItemEdit2";
            this.btn_ItemEdit2.Size = new System.Drawing.Size(36, 23);
            this.btn_ItemEdit2.TabIndex = 50;
            this.btn_ItemEdit2.Text = "Edit";
            this.btn_ItemEdit2.UseVisualStyleBackColor = true;
            this.btn_ItemEdit2.Click += new System.EventHandler(this.btn_ItemEdit2_Click);
            // 
            // btn_ItemEdit3
            // 
            this.btn_ItemEdit3.Location = new System.Drawing.Point(495, 219);
            this.btn_ItemEdit3.Name = "btn_ItemEdit3";
            this.btn_ItemEdit3.Size = new System.Drawing.Size(36, 23);
            this.btn_ItemEdit3.TabIndex = 51;
            this.btn_ItemEdit3.Text = "Edit";
            this.btn_ItemEdit3.UseVisualStyleBackColor = true;
            this.btn_ItemEdit3.Click += new System.EventHandler(this.btn_ItemEdit3_Click);
            // 
            // btn_ItemEdit4
            // 
            this.btn_ItemEdit4.Location = new System.Drawing.Point(495, 304);
            this.btn_ItemEdit4.Name = "btn_ItemEdit4";
            this.btn_ItemEdit4.Size = new System.Drawing.Size(36, 23);
            this.btn_ItemEdit4.TabIndex = 52;
            this.btn_ItemEdit4.Text = "Edit";
            this.btn_ItemEdit4.UseVisualStyleBackColor = true;
            this.btn_ItemEdit4.Click += new System.EventHandler(this.btn_ItemEdit4_Click);
            // 
            // btn_ItemDelete1
            // 
            this.btn_ItemDelete1.Location = new System.Drawing.Point(537, 54);
            this.btn_ItemDelete1.Name = "btn_ItemDelete1";
            this.btn_ItemDelete1.Size = new System.Drawing.Size(55, 23);
            this.btn_ItemDelete1.TabIndex = 53;
            this.btn_ItemDelete1.Text = "Delete";
            this.btn_ItemDelete1.UseVisualStyleBackColor = true;
            this.btn_ItemDelete1.Click += new System.EventHandler(this.btn_ItemDelete1_Click);
            // 
            // btn_ItemDelete2
            // 
            this.btn_ItemDelete2.Location = new System.Drawing.Point(537, 135);
            this.btn_ItemDelete2.Name = "btn_ItemDelete2";
            this.btn_ItemDelete2.Size = new System.Drawing.Size(55, 23);
            this.btn_ItemDelete2.TabIndex = 54;
            this.btn_ItemDelete2.Text = "Delete";
            this.btn_ItemDelete2.UseVisualStyleBackColor = true;
            this.btn_ItemDelete2.Click += new System.EventHandler(this.btn_ItemDelete2_Click);
            // 
            // btn_ItemDelete3
            // 
            this.btn_ItemDelete3.Location = new System.Drawing.Point(537, 219);
            this.btn_ItemDelete3.Name = "btn_ItemDelete3";
            this.btn_ItemDelete3.Size = new System.Drawing.Size(55, 23);
            this.btn_ItemDelete3.TabIndex = 55;
            this.btn_ItemDelete3.Text = "Delete";
            this.btn_ItemDelete3.UseVisualStyleBackColor = true;
            this.btn_ItemDelete3.Click += new System.EventHandler(this.btn_ItemDelete3_Click);
            // 
            // btn_ItemDelete4
            // 
            this.btn_ItemDelete4.Location = new System.Drawing.Point(537, 304);
            this.btn_ItemDelete4.Name = "btn_ItemDelete4";
            this.btn_ItemDelete4.Size = new System.Drawing.Size(55, 23);
            this.btn_ItemDelete4.TabIndex = 56;
            this.btn_ItemDelete4.Text = "Delete";
            this.btn_ItemDelete4.UseVisualStyleBackColor = true;
            this.btn_ItemDelete4.Click += new System.EventHandler(this.btn_ItemDelete4_Click);
            // 
            // nud_ItemStock2
            // 
            this.nud_ItemStock2.Location = new System.Drawing.Point(195, 135);
            this.nud_ItemStock2.Name = "nud_ItemStock2";
            this.nud_ItemStock2.Size = new System.Drawing.Size(46, 20);
            this.nud_ItemStock2.TabIndex = 57;
            // 
            // nud_ItemStock3
            // 
            this.nud_ItemStock3.Location = new System.Drawing.Point(195, 219);
            this.nud_ItemStock3.Name = "nud_ItemStock3";
            this.nud_ItemStock3.Size = new System.Drawing.Size(46, 20);
            this.nud_ItemStock3.TabIndex = 58;
            // 
            // nud_ItemStock4
            // 
            this.nud_ItemStock4.Location = new System.Drawing.Point(195, 304);
            this.nud_ItemStock4.Name = "nud_ItemStock4";
            this.nud_ItemStock4.Size = new System.Drawing.Size(46, 20);
            this.nud_ItemStock4.TabIndex = 59;
            // 
            // btn_RefreshPage
            // 
            this.btn_RefreshPage.Location = new System.Drawing.Point(12, 53);
            this.btn_RefreshPage.Name = "btn_RefreshPage";
            this.btn_RefreshPage.Size = new System.Drawing.Size(129, 35);
            this.btn_RefreshPage.TabIndex = 23;
            this.btn_RefreshPage.Text = "Refresh Page";
            this.btn_RefreshPage.UseVisualStyleBackColor = true;
            this.btn_RefreshPage.Click += new System.EventHandler(this.btn_RefreshPage_Click);
            // 
            // f_InventoryManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(761, 440);
            this.Controls.Add(this.btn_RefreshPage);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.tl_Pages);
            this.Controls.Add(this.btn_NextPage);
            this.Controls.Add(this.btn_PreviousPage);
            this.Controls.Add(this.gb_SortByGroup);
            this.Controls.Add(this.btn_AddItem);
            this.Name = "f_InventoryManager";
            this.Text = "Inventory Manager";
            this.Load += new System.EventHandler(this.f_InventoryManager_Load);
            this.gb_SortByGroup.ResumeLayout(false);
            this.gb_SortByGroup.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nud_ItemStock1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_ItemStock2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_ItemStock3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_ItemStock4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_AddItem;
        private System.Windows.Forms.RadioButton rb_SortLowPrice;
        private System.Windows.Forms.RadioButton rb_SortHighPrice;
        private System.Windows.Forms.RadioButton rb_HighestStock;
        private System.Windows.Forms.RadioButton rb_LowestStock;
        private System.Windows.Forms.RadioButton rb_Lightest;
        private System.Windows.Forms.RadioButton rb_Heaviest;
        private System.Windows.Forms.RadioButton rb_Smallest;
        private System.Windows.Forms.RadioButton rb_Biggest;
        private System.Windows.Forms.GroupBox gb_SortByGroup;
        private System.Windows.Forms.Button btn_PreviousPage;
        private System.Windows.Forms.Button btn_NextPage;
        private System.Windows.Forms.Label tl_Pages;
        private System.Windows.Forms.Label tl_Name;
        private System.Windows.Forms.Label tl_Price;
        private System.Windows.Forms.Label tl_Stock;
        private System.Windows.Forms.Label tl_Weight;
        private System.Windows.Forms.Label tl_Size;
        private System.Windows.Forms.Label tl_Description;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label tl_ItemName1;
        private System.Windows.Forms.Label tl_ItemName2;
        private System.Windows.Forms.Label tl_ItemName3;
        private System.Windows.Forms.Label tl_ItemName4;
        private System.Windows.Forms.Label tl_ItemPrice1;
        private System.Windows.Forms.Label tl_ItemPrice2;
        private System.Windows.Forms.Label tl_ItemPrice3;
        private System.Windows.Forms.Label tl_ItemPrice4;
        private System.Windows.Forms.Label tl_ItemWeight1;
        private System.Windows.Forms.Label tl_ItemWeight2;
        private System.Windows.Forms.Label tl_ItemWeight3;
        private System.Windows.Forms.Label tl_ItemWeight4;
        private System.Windows.Forms.Label tl_ItemSize1;
        private System.Windows.Forms.Label tl_ItemSize2;
        private System.Windows.Forms.Label tl_ItemSize3;
        private System.Windows.Forms.Label tl_ItemSize4;
        private System.Windows.Forms.Label tl_ItemDescription1;
        private System.Windows.Forms.Label tl_ItemDescription2;
        private System.Windows.Forms.Label tl_ItemDescription3;
        private System.Windows.Forms.Label tl_ItemDescription4;
        private System.Windows.Forms.Label tl_Edit;
        private System.Windows.Forms.Label tl_Delete;
        private System.Windows.Forms.Button btn_ItemEdit1;
        private System.Windows.Forms.Button btn_ItemEdit2;
        private System.Windows.Forms.Button btn_ItemEdit3;
        private System.Windows.Forms.Button btn_ItemEdit4;
        private System.Windows.Forms.Button btn_ItemDelete1;
        private System.Windows.Forms.Button btn_ItemDelete2;
        private System.Windows.Forms.Button btn_ItemDelete3;
        private System.Windows.Forms.Button btn_ItemDelete4;
        private System.Windows.Forms.Button btn_RefreshPage;
        private System.Windows.Forms.NumericUpDown nud_ItemStock1;
        private System.Windows.Forms.NumericUpDown nud_ItemStock2;
        private System.Windows.Forms.NumericUpDown nud_ItemStock3;
        private System.Windows.Forms.NumericUpDown nud_ItemStock4;
        private System.Windows.Forms.RadioButton rb_Alphabetics;
    }
}