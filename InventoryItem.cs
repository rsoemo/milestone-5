﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Milestone_4
{
	public class InventoryItem
	{
		//==============================//
		//=======Object=Variables=======//
		//==============================//
		private String itemName;
		private double itemPrice;
		private int itemStock;
		private double itemWeight;
		private double itemSize;
		private String itemDescription;

		//=========================//
		//=======Constructor=======//
		//=========================//
		public InventoryItem(String itemName, double itemPrice, int itemStock, double itemWeight, double itemSize, String itemDescription)
		{
			this.itemName = itemName;
			this.itemPrice = itemPrice;
			this.itemStock = itemStock;
			this.itemWeight = itemWeight;
			this.itemSize = itemSize;
			this.itemDescription = itemDescription;
		}

		//=====================//
		//=======Getters=======//
		//=====================//
		//===getItemName===//
		public String getItemName()
		{
			return itemName;
		}
		//===getItemPrice===//
		public double getItemPrice()
		{
			return itemPrice;
		}
		//===getItemStock===//
		public int getItemStock()
		{
			return itemStock;
		}
		//===getItemWeight===//
		public double getItemWeight()
		{
			return itemWeight;
		}
		//===getItemSize===//
		public double getItemSize()
		{
			return itemSize;
		}
		//===getItemDescription===//
		public String getItemDescription()
		{
			return itemDescription;
		}

		//=====================//
		//=======Setters=======//
		//=====================//
		//===setItemName===//
		public void setItemName(String itemName)
		{
			this.itemName = itemName;
		}
		//===setItemPrice===//
		public void setItemPrice(double itemPrice)
		{
			this.itemPrice = itemPrice;
		}
		//===setItemStock===//
		public void setItemStock(int itemStock)
		{
			this.itemStock = itemStock;
		}
		//===setItemWeight===//
		public void setItemWeight(double itemWeight)
		{
			this.itemWeight = itemWeight;
		}
		//===setItemSize===//
		public void setItemSize(double itemSize)
		{
			this.itemSize = itemSize;
		}
		//===setItemDescription===//
		public void setItemDescription(String itemDescription)
		{
			this.itemDescription = itemDescription;
		}

		//===========================//
		//=======Stock=Changes=======//
		//===========================//
		//===addItemStock===//
		public void addItemStock(int itemsToAdd)
		{
			itemStock += itemsToAdd;
		}
		//===addItemStock===//
		public void removeItemStock(int itemsToRemove)
		{
			itemStock -= itemsToRemove;
		}

		//=======================//
		//=======CompareTo=======//
		//=======================//
		public int CompareTo(InventoryItem ii2, InventoryItemComparer.ComparisonType comparisonType)
		{
			switch (comparisonType)
			{
				case InventoryItemComparer.ComparisonType.itemName:  //Comparison Type
					return itemName.CompareTo(ii2.getItemName());  //Calling Comparison Type
				case InventoryItemComparer.ComparisonType.itemPrice:
					return itemPrice.CompareTo(ii2.getItemPrice());
				case InventoryItemComparer.ComparisonType.itemStock:
					return itemStock.CompareTo(ii2.getItemStock());
				case InventoryItemComparer.ComparisonType.itemWeight:
					return itemWeight.CompareTo(ii2.getItemWeight());
				case InventoryItemComparer.ComparisonType.itemSize:
					return itemSize.CompareTo(ii2.getItemSize());
				default:
					return itemName.CompareTo(ii2.getItemName());
			}
		}
	}
}