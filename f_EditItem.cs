﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Milestone_4
{
    public partial class f_EditItem : Form
    {
        //=============================//
        //=======Class=Variables=======//
        //=============================//
        InventoryItem editingItem;  //The item being edited
        Boolean isEditingItem;  //If this class is editing an item object

        //=========================//
        //=======Constructor=======//
        //=========================//
        public f_EditItem(InventoryItem editingItemInput, Boolean isEditingItemInput)
        {
            this.isEditingItem = isEditingItemInput;  //Storing inputed boolean
            this.editingItem = editingItemInput;  //Storing inputed item to object
            InitializeComponent();  //Initializing
        }

        //==============================//
        //=======When=Form=Loaded=======//
        //==============================//
        private void f_EditItem_Load(object sender, EventArgs e)
        {
            //======If=Editing=Existing=Item======//
            if (isEditingItem == true)
            {
                //===Pushing=Properties=Of=Items=To=Form===//
                this.Text = "Edit " + editingItem.getItemName();  //Title of Form
                this.tb_ItemNameEdit.Text = editingItem.getItemName();  //Name Box
                this.nud_ItemPriceEdit.Text = editingItem.getItemPrice().ToString();  //Price Box
                //this.nud_ItemStockEdit.Text = editingItem.getItemStock().ToString();  //Stock Box
                this.nud_ItemStockEdit.Visible = false;  //Stock Box
                this.tl_ItemStock.Visible = false;  //Stock Box
                this.nud_ItemWeightEdit.Text = editingItem.getItemWeight().ToString();  //Weight Box
                this.nud_ItemSizeEdit.Text = editingItem.getItemSize().ToString();  //Size Box
                this.tb_ItemDescriptionEdit.Text = editingItem.getItemDescription().ToString();  //Description Box
            }
            //======If=Creating=New=Item======//
            else if (isEditingItem == false)
            {
                //===Pushing=Properties=Of=Items=To=Form===//
                this.Text = "Adding New Item";  //Title of Form
            }
        }

        //=========================//
        //=======Edits=Saved=======//
        //=========================//
        private void btn_SaveEdits_Click(object sender, EventArgs e)
        {
            //======If=Editing=Existing=Item======//
            if (isEditingItem == true)
            {
                //===Saving=Properties===//
                editingItem.setItemName(tb_ItemNameEdit.Text.ToString());  //Setting Name
                editingItem.setItemPrice(Double.Parse(nud_ItemPriceEdit.Value.ToString()));  //Setting Price
                editingItem.setItemStock(int.Parse(nud_ItemStockEdit.Value.ToString()));  //Setting Stock
                editingItem.setItemWeight(Double.Parse(nud_ItemWeightEdit.Value.ToString()));  //Setting Weight
                editingItem.setItemSize(Double.Parse(nud_ItemSizeEdit.Value.ToString()));  //Setting Size
                editingItem.setItemDescription(tb_ItemDescriptionEdit.Text.ToString());  //Setting Description
            }
            //======If=Creating=New=Item======//
            else if (isEditingItem == false)
            {
                //===Saving=Properties===//
                String name = tb_ItemNameEdit.Text.ToString();  //Setting Name
                Double price = Double.Parse(nud_ItemPriceEdit.Value.ToString());  //Setting Price
                int stock = int.Parse(nud_ItemStockEdit.Value.ToString());  //Setting Stock
                Double weight = Double.Parse(nud_ItemWeightEdit.Value.ToString());  //Setting Weight
                Double size = Double.Parse(nud_ItemSizeEdit.Value.ToString());  //Setting Size
                String description = tb_ItemDescriptionEdit.Text.ToString();  //Setting Description

                InventoryItem newItem = new InventoryItem(name, price, stock, weight, size, description);  //Storing variables in object
                Program.inventory.addInventoryItem(newItem);  //Storing object in inventory
            }
            //===Closing=Window===//
            this.Close();  //Closing window form
        }

        //============================//
        //=======Edits=Canceled=======//
        //============================//
        private void btn_CancelEdits_Click(object sender, EventArgs e)
        {
            this.Close();  //Closing window form
        }
    }
}