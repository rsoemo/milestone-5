﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Milestone_4
{
    class InventoryManager
    {
        //=============================//
        //=======Class=Variables=======//
        //=============================//
        private static List<InventoryItem> inventoryItemList = new List<InventoryItem>();
        readonly private static InventoryItem blank = new InventoryItem("", 0, 0, 0, 0, "");
        private static int pages;

        //==============================//
        //=======addInventoryItem=======//
        //==============================//
        public void addInventoryItem(InventoryItem newItem)
        {
            inventoryItemList.Add(newItem);
        }

        //=================================//
        //=======removeInventoryItem=======//
        //=================================//
        public void removeInventoryItem(InventoryItem removedItem)
        {
            inventoryItemList.Remove(removedItem);
        }

        //==================================//
        //=======restockInventoryItem=======//
        //==================================//
        public void restockInventoryItem(InventoryItem item, int numberOfNew)
        {
            InventoryItem itemInList = findSpecificItem(item);  //Finding item in list
            if (itemInList != null)
            {
                itemInList.addItemStock(numberOfNew);  //Updating entry
            }
        }

        //======================================//
        //=======displayAllInventoryItems=======//
        //======================================//
        public void displayAllInventoryItems()
        {
            for (int currentItem = 0; currentItem < inventoryItemList.Count; currentItem++)  //Looping through list of items
            {
                InventoryItem ci = inventoryItemList[currentItem];  //Defining current item for ease of use
                Console.WriteLine("-> " + ci.getItemName() + " for $" + ci.getItemPrice() + " with " + ci.getItemStock() + " left in stock: " + ci.getItemDescription());  //Printing inventory item
            }
        }

        //===========================//
        //=======searchForItem=======//
        //===========================//
        public List<InventoryItem> searchForItem(String nameSearch, int amountSearch)
        {
            List<InventoryItem> searchHits = new List<InventoryItem>();  //Creating a search hit list

            //======Searching======//
            //===If=Name=Is=Searched===//
            if (nameSearch != null)  //If nameSearch not empty
            {
                for (int currentItem = 0; currentItem < inventoryItemList.Count; currentItem++)  //Looping through list of items
                {
                    if (inventoryItemList[currentItem].getItemName().ToLower().CompareTo(nameSearch.ToLower()) == 0)  //Checking if names are the same
                    {
                        searchHits.Add(inventoryItemList[currentItem]);  //Adding hit to list
                    }
                }
            }
            //===If=Amount=Is=Searched===//
            else if (amountSearch != 0)
            {
                for (int currentItem = 0; currentItem < inventoryItemList.Count; currentItem++)  //Looping through list of items
                {
                    if (inventoryItemList[currentItem].getItemStock() == amountSearch)  //Checking if stock counts are the same
                    {
                        searchHits.Add(inventoryItemList[currentItem]);  //Adding hit to list
                    }
                }
            }

            //======Returning======//
            //===If=There=Are=No=Hits===//
            if (searchHits.Count == 0)
            {
                return null;  //Returning null, because nothing found
            }
            //===If=There=Are=Hits===//
            else
            {
                return searchHits;  //Returning the list of hits
            }
        }

        //==============================//
        //=======findSpecificItem=======//
        //==============================//
        private InventoryItem findSpecificItem(InventoryItem itemSearch)
        {
            //======If=Item=Is=Searched======//
            if (itemSearch != null)  //If itemSearch not empty
            {
                for (int currentItem = 0; currentItem < inventoryItemList.Count; currentItem++)  //Looping through list of items
                {
                    //===Checking=If=The=Item=Is=Exactly=The=Same===//
                    //This is an attempt to stop mix ups with duplicated items//
                    if (inventoryItemList[currentItem].getItemName().CompareTo(itemSearch.getItemName()) == 0 &&  //Checking if the names are the same
                        inventoryItemList[currentItem].getItemDescription().CompareTo(itemSearch.getItemDescription()) == 0 &&  //Checking if descriptions are the same
                        inventoryItemList[currentItem].getItemPrice() == itemSearch.getItemPrice() &&  //Checking if prices are the same
                        inventoryItemList[currentItem].getItemSize() == itemSearch.getItemSize() &&  //Checking if sizes are the same
                        inventoryItemList[currentItem].getItemStock() == itemSearch.getItemStock() &&  //Checking if stocks are same
                        inventoryItemList[currentItem].getItemWeight() == itemSearch.getItemWeight())  //Checking if weights are the same
                    {
                        return inventoryItemList[currentItem];  //Returning match
                    }
                }
                return null;  //Nothing found, so returning a null
            }
            else
            {
                return null;  //Nothing entered, so returning a null
            }
        }

        //=========================//
        //=======pullForMenu=======//
        //=========================//
        public List<InventoryItem> pullForMenu(int page)
        {
            //======Creating=Variables=======//
            int calculatingNumber = page - 1;  
            List<InventoryItem> pageContent = new List<InventoryItem>();

            //======Getting=Content=For=Page======//
            //===Adding=Item=One===//
            try
            {
                pageContent.Add(inventoryItemList[0 + (4 * calculatingNumber)]);
            }
            catch (ArgumentOutOfRangeException e)
            {
                pageContent.Add(blank);
            }
            //===Adding=Item=Two===//
            try
            {
                pageContent.Add(inventoryItemList[1 + (4 * calculatingNumber)]);
            }
            catch (ArgumentOutOfRangeException e)
            {
                pageContent.Add(blank);
            }
            //===Adding=Item=Three===//
            try
            {
                pageContent.Add(inventoryItemList[2 + (4 * calculatingNumber)]);
            }
            catch (ArgumentOutOfRangeException e)
            {
                pageContent.Add(blank);
            }
            //===Adding=Item=Four===//
            try
            {
                pageContent.Add(inventoryItemList[3 + (4 * calculatingNumber)]);
            }
            catch (ArgumentOutOfRangeException e)
            {
                pageContent.Add(blank);
            }

            //======Returning=List======//
            return pageContent;
        }

        //===============================//
        //=======generatePageCount=======//
        //===============================//
        public void generatePageCount()
        {
            if ((inventoryItemList.Count % 4) != 0)  //If the total count has a remainder after being devided by 4
            {
                pages = (inventoryItemList.Count / 4) + 1;
            }
            else
            {
                pages = (inventoryItemList.Count / 4);
            }
        }

        //============================//
        //=======Get=Page=Count=======//
        //============================//
        public int getPageCount()
        {
            return pages;
        }

        //==========================//
        //=======Sort=Methods=======//
        //==========================//
        //=======sortAlphabetically=======//
        public void sortAlphabetically()
        {
            //===Sorting===//
            Program.itemcomparer.ComparisonMethod = InventoryItemComparer.ComparisonType.itemName;  //Setting up comparison
            inventoryItemList.Sort(Program.itemcomparer);  //Executing comparison
        }

        //=======sortByPrice=======//
        public void sortByPrice(Boolean sortBySmallest)
        {
            //===Sorting=By=Price===//
            Program.itemcomparer.ComparisonMethod = InventoryItemComparer.ComparisonType.itemPrice;  //Setting up comparison
            inventoryItemList.Sort(Program.itemcomparer);  //Executing comparison

            //===Resorting=For=Size===//
            if (sortBySmallest == false)
            {
                inventoryItemList.Reverse();  //Reversing list
            }
        }

        //=======sortByStock=======//
        public void sortByStock(Boolean sortBySmallest)
        {
            //===Sorting=By=Stock===//
            Program.itemcomparer.ComparisonMethod = InventoryItemComparer.ComparisonType.itemStock;  //Setting up comparison
            inventoryItemList.Sort(Program.itemcomparer);  //Executing comparison

            //===Resorting=For=Size===//
            if (sortBySmallest == false)
            {
                inventoryItemList.Reverse();  //Reversing list
            }
        }

        //=======sortByWeight=======//
        public void sortByWeight(Boolean sortBySmallest)
        {
            //===Sorting=By=Weight===//
            Program.itemcomparer.ComparisonMethod = InventoryItemComparer.ComparisonType.itemWeight;  //Setting up comparison
            inventoryItemList.Sort(Program.itemcomparer);  //Executing comparison

            //===Resorting=For=Size===//
            if (sortBySmallest == false)
            {
                inventoryItemList.Reverse();  //Reversing list
            }
        }

        //=======sortBySize=======//
        public void sortBySize(Boolean sortBySmallest)
        {
            //===Sorting=By=Size===//
            Program.itemcomparer.ComparisonMethod = InventoryItemComparer.ComparisonType.itemSize;  //Setting up comparison
            inventoryItemList.Sort(Program.itemcomparer);  //Executing comparison

            //===Resorting=For=Size===//
            if (sortBySmallest == false)
            {
                inventoryItemList.Reverse();  //Reversing list
            }
        }
    }
}