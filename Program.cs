﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Milestone_4
{
    static class Program
    {
        //=============================//
        //=======Class=Variables=======//
        //=============================//
        public static InventoryManager inventory = new InventoryManager();  //Creating a public static IM for the program to reference
        public static List<InventoryItem> pageList = new List<InventoryItem>();  //Creating a public static list of the current page for the program to reference
        public static InventoryItemComparer itemcomparer = new InventoryItemComparer();  //Creating a public static item comparer

        //==================//
        //=======Main=======//
        //==================//
        [STAThread]
        static void Main()
        {
            //===Creating=Data===//
            createStartData();
            
            //======CreatingForm======//
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            f_InventoryManager inventoryWindow = new f_InventoryManager();
            Application.Run(inventoryWindow);
        }

        //=============================//
        //=======createStartData=======//
        //=============================//
        static void createStartData()
        {
            //===Creating=Data===//
            InventoryItem item1 = new InventoryItem("Headset", 20.50, 15, 2.3, 1.2, "Standard headset");
            InventoryItem item2 = new InventoryItem("Headset Pro", 49.8, 7, 3.0, 2.3, "Pro headset");
            InventoryItem item3 = new InventoryItem("Keyboard", 15.99, 10, 5.3, 5.0, "Standard keyboard");
            InventoryItem item4 = new InventoryItem("Mouse", 10.50, 12, 3.2, 1.2, "Standard mouse");
            InventoryItem item5 = new InventoryItem("Mouse Pro", 20.99, 2, 3.8, 2.1, "Pro mouse");

            //===Adding=Data===//
            inventory.addInventoryItem(item1);
            inventory.addInventoryItem(item2);
            inventory.addInventoryItem(item3);
            inventory.addInventoryItem(item4);
            inventory.addInventoryItem(item5);
        }
    }
}